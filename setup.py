
from distutils.core import setup
setup(
        dependency_links = [
            "https://bitbucket.org/drumchaps/chaps_os_utils/get/master.zip#egg=chaps-os-utils-0.0.1",
        ],
        name='proxyrentalapi',
        version='0.1.0',
        author="Chaps",
        author_email="drumchaps@gmail.com",
        maintainer="Chaps",
        maintainer_email="drumchaps@gmail.com",
        url="https://bitbucket.org/drumchaps/proxyrentalapi",
        py_modules=[
        ],   
        packages  = [
            "proxyrentalapi",
        ],
        package_dir={'proxyrentalapi': 'src/proxyrentalapi'},
        install_requires = [
            "requests",
            "chaps-os-utils"
        ],

)
